module gitlab.com/gitlab-org/gitlab-shell/go

go 1.12

require (
	github.com/mattn/go-shellwords v0.0.0-20190425161501-2444a32a19f4
	github.com/otiai10/copy v1.0.1
	github.com/otiai10/curr v0.0.0-20190513014714-f5a3d24e5776 // indirect
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.3.0
	gitlab.com/gitlab-org/gitaly v1.68.0
	gitlab.com/gitlab-org/labkit v0.0.0-20190221122536-0c3fc7cdd57c
	google.golang.org/grpc v1.24.0
	gopkg.in/DataDog/dd-trace-go.v1 v1.9.0 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
